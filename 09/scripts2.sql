--Mekteb adi	Mekteb unvan	Sinif	Muellim adi	Muellim Soyad	Muellim Tevellud	Shagird adi	Shagird Soyad	Shagird Tevellud	Shagird Qiymet	Tarix

create table  melumatlar
(mekteb_adi varchar2(100),
 mekteb_unvan varchar2(100),
 sinif varchar2(100),
 muellim_adi varchar2(100),
 muellim_soyad varchar2(100),
 tevellud varchar2(100),
 shagird_adi varchar2(100),
 shagird_soyad varchar2(100),
 shagird_tevellud varchar2(100),
 shagirs_qiymet varchar2(100),
 qiymet_tarix varchar2(100));
 
 
 --Mekteb haqda melumatlar cedveline melumat daxil edirem. Misal olaraq, bu sistem yeni  istifade verilmis mekteb ucundur, harada ki, 
 --tehsil alacaq shagirdler ve ders deyecek muellimler teyin edilmeyibdir. Mekteb adi da yeni verilibdir ve bu yeni adi 
 --cedvele daxil etmek lazimdir. Bu melumatlar mekteb_adi ve mekteb_unvan sutunlarinda saxlanmalidirlar.
 --
 insert into melumatlar(mekteb_adi,mekteb_unvan) values('1 sayli orta mekteb','A.Ismailov kuc 1');
 commit;
 --elave edilmis mektebin melumatlarina baxaq
 select * from melumatlar;
 
--mekteb_adi ve mekteb_unvan sutunlarindan basqa diger sutunlarda null deyeri olacaqdir. Null o demekdir ki, bu xanalara her hansi melumat elave edilmeyibdir.
--Heqiqeten de, biz  21-ci setrde yalniz 2 sutuna melumatlari elave etdik.

--daha sonra mektebe yeni muellim teyin olundugu ve bu muellimin melumatlarini melumatlar cedveline daxil etmeyimiz bizden teleb olunur.
--burada 2 hali nezerden kecirek:
--1. yalniz muellime aid melumatlari daxil ede bilerem. Bu zaman yeni yaranacaq setrde yalniz muellim melumatlari olacaq ve biz bu halda bu mueelimin hansi mektebe
--bagli oldugunu teyin ede bilmeyeceyik.
insert into melumatlar(muellim_adi,muellim_soyad,tevellud) values('Ehmed','Ehmedov','01/01/1980');
commit;
--Elave etdiyimiz melumatlara baxaq.
select * from melumatlar;
--Cedveldeki 2-ci setre baxsaq gorerik ki, qeyd etdiyimiz kimi, muellim qarsisinda  olan mekteb melumatlari null -dur. Cunki biz bu melumatlari daxil etmedik. 
--bunu aradan qaldirmaq ucun, yani hansi muellim hansi mektebe bagli oldugunu mueyyen etmek ucun , elave olaraq,  mekteb melumatlarini da  birge elave edek.
--2. Mekteb ve Muellim melumatlarini birge elave edek.
--ilk once Ehmed m ucun elave etdiyimiz melumatlari silek. beleki hemin str biz lazim deyil.Yada saliriq ki, string tiple melumatlarla isledikde, simvollarin boyuk ve ya 
--kicik yazilmasi onem dasiyir. Yani adi Ehmed olan setri silmek istesem mutelq olaraq Ehmed yazmaliyam ehmed ve ya EHMED deyil. Eks halda hemin deyere gore silmek istediyi
--setri tapmayacaq ve netice setr silinmeyecek.
delete from melumatlar where muellim_adi='Ehmed';
commit;
--neticeni yoxlayiriq.
select * from melumatlar;
--setr silinib ve biz yeni setr elave edirik harada ki, mekteb ve muellim melumatlarini ozunde cemleyir.
insert into melumatlar(mekteb_adi,mekteb_unvan,muellim_adi,muellim_soyad,tevellud) 
  values('1 sayli orta mekteb','A.Ismailov kuc 1','Ehmed','Ehmedov','01/01/1980');
commit;
--melumatlari yoxlayaq
select * from melumatlar;
--2-ci setrde biz Ehmed muellimin hansi mektebe aid oldugunu goruruk evvelki silinmis setrden ferqli olaraq. Amma burada bir problem var.
--biz mekteb melumatlarini tekrarlamis olduq. Elbetde deye bilersiniz ki, veziyyetden cixis yolu olaraq, 1-ci setrdeki, melumatlari silek ve bu hell yolu ola biler.
--Eger yeni muellim elave etsek o zaman nece? ya da Ehmed m mektebden isden cixsa, yani 2-ci setr silinse nece? Axi bu halda biz mekteb melumatlarini umumiyyetle itireceyik.
--Butun bunlara eyani baxaq:
--1. 1-ci setri silek, tekrarlanmani aradan qaldiraq ve neticeye baxaq. Bunun ucun cedveldeki mehz birinci setri silmek ucun, sorgu yazmaliyiq.
--Bunu muellim_adi sutununa gore edirem, deyirem ki, haradaki muellim_adi sutunu null-dur hemin setri sil.
--eger bu sorgu bu sekilde yazsam xeta alacam
delete from melumatlar where muellim_adi=NULL ;
commit;
--ve ya bu sekilde yazsam da xeta verecekdir
delete from melumatlar where muellim_adi='null';
commit;
--Xeta vermek sebebi odur ki, sutun deyeri null olan setrleri filter etmek ucun  ferqli sekilde  yazilmalidir. mehz bu sekilde yazsaq xeta vermeyecek
delete from melumatlar where muellim_adi is null;
commit;
--Bu yazilis yalniz NULL deyerleri ucun gecerlidir!
--Neticeni yoxlayaq.
select * from melumatlar;
--Burada biz artiq bir setri goruruk ve hesab edirik ki, mekteb melumatlarinin yani mekteb_adi ve mekteb_unvan melumatlarinin tekrarlanmasini araqdan qaldirdir.
--Eger Ehmed m 1 sayli mektebden isden cixarsa biz ona melumatlari silmek zeruretinde olacagiq. Eger Ehmed m aid melumati silsek bizim melumat cedvelinde
--hemcinin de mektebe aid melumatlar da silinecek .Neticede bizim en once bize verilen tapsiriqi yani mekteb melumatlarinin daxil edilmesini yerine yetirmemis kimi
--olacagiq. 
--Bu deyilenleri  gormek ucun ,nezere alaq ki, Ehmed m isden cixib ve onun melumatlari silinmelidir.
delete from melumatlar where muellim_adi='Ehmed';
commit;
--neticeni yoxlayaq.
select * from melumatlar;
--artiq cedvelde hec bir melumatin olmadigini goruruk.
--Mekteb melumatlarinin bu sekilde saxlanmasinin problemler yaratdigini eyani gorduk ve bu problemin helli ucun oyrendiyimiz bilikleri tetbiq etmeye calisacagiq.
--ilk once mekteb melumatlarinin diger melumatlardan asilligini aradan qaldirmaq ucun, tedbriler heyata kecirmek lazimdir.
--Ilk once melumatlar cedvelini yeniden formalasdiraraq orada mektebe aid olan sutunlari silmek ve daha sonra ise YALNIZ mekteb melumatlarini ozunde saxlayan,
--YENI cedvel yaratmaq
--1. melumatlar cedvelinden mekteb_adi ve mekteb_unvan sutunlari silmek
--her 2 sutunu eyni zaman silmeye cehd etsem xeta verecek.
alter table melumatlar drop column mekteb_adi,mekteb_unvan;
--ancaq tek tek silmek ucun ayr-ayri emrler vermek lazimdir ki, neticeni elde edek
alter table melumatlar drop column mekteb_adi;
alter table melumatlar drop column mekteb_unvan;
--neticeni yoxlayiriq
select * from melumatlar;
--mektebe aid sutunlar gorunmur.
--2.Mekteb melumatlari ozunde saxlayan mekteb adi cedveli yaratmaq
create table mekteb
(mekteb_adi varchar2(100),
 mekteb_unvan varchar2(100));
 
--Neticeni yoxlayaq
select * from mekteb;

--mekteb melumatini artiq yeni yaratdigimiz cedvele elave edek.
insert into mekteb(mekteb_adi,mekteb_unvan) values('1 sayli orta mekteb','A.Ismailov kuc 1');
commit;
--Neticeni yoxlayaq
select * from mekteb;
--Bir muddet sonra Ehmed m evezine yeni muellim geldi ve bu muellimi melumatlar cedveline elave etmek lazimdir;
--Melumatlari elave etmek istedikde , yadimiza dusur ki, axi melumatlar cedvelinde biz meketebe aid sutunlari oradan silmisdik,
--ve yeni elave edeceyimiz muellim melumatlarinin mekteble elaqelendirilmesi bu sebebden mumkun olmayacaqdir.
--Buna gore de yeni yaratdigimiz mekteb ve cari melumatlar cedvelini elaqelendirmek lazimdir.
--Burada biz neyin  neden asili oldugunu mueyyen etdik den sonra  Entity Relationship- in novlerinden birini tetbiq edeceyik.
--Muellimlerin mektebden asili oldugunu nezere alsaq , melumatlar cedveli ozunde mekteb cedveline istinad etmelidir. Buna gore de,
--biz melumatlar cedvelinde istinad eden sutun mekteb_id ve istinad olunacaq cedvelde -mekteb -de id adli yeni sutunlari yaratmaliyiq. 
--biz bu sutunlari cedvelleri silerek create emrlerinde yeni sutunlari nezere alaraq da , yarada bilerik ,ya da movcud cedvellerde deyisiklik ederek de,
--yarada bilerik. Taklif edirem, 2-ci ni tetbiq edek.
--1. melumatlar cedvelinde yeni sutuna elave edek
alter table melumatlar add  mekteb_id varchar2(100);
--neticeni yoxlayaq
select * from melumatlar;
--en sonda bu yeni sutunu gormek olar. Gorduyumuz kimi, her yeni elave olunan sutun , movcud olan sutunlardan sonra yaradilir.
--2. mekteb cedveline yeni sutun elave edek.
alter table mekteb add id varchar2(100);
--neticeni yoxlayaq
select * from mekteb;
--burada hemcinin yeni sutun son yerde eks olunur. ve her iki sutun elave etdikden sonra biz  yeni sutunlarda null ifadesini goruruk. 
--yani hemin sutunlarda melumatlarin olmamasini bize bildirir.
--Gerekli olan sutunlari elave etdikden sonra, esas mesele olan , 2 cedvel arasindaki elaqeleri qurmaq ucun, yeni sutunlarda bu elaqeleri temin eden deyerleri
--elave etmek lazimdir. Yani mekteb cedvelinde olan ID sutuna 1 deyer  bu deyeri ozunde hemcinin saxlayan melumatlar cedvelindeki mekteb_id sutunlarina elave etmek lazimdir.
--1.Mekteb cedveline elave etmek ucun sorgu yazaq
update mekteb set id=1 where mekteb_adi='1 sayli orta mekteb';
commit;
--neticeleri yoxlayaq
select * from mekteb;
--burada biz ID sutununda 1 deyerini goruruk
--2.melumatlar cedveline elave etmek ucun sorgu yazaq
update melumatlar set mekteb_id=1 where ????? --axi  bu cedvelde bir melumat yoxdur ki, onu redakte edek. Cunki Ehmed m silmisdik ve yeni muellimi elave
--etmeye furset olmamisdir. Buna gore de ilk olaraq yeni muellimi elave edek
--biz mekteb_id sutuna 1 deyerini yeni muellimin melumatlarini daxil etdiyimizde de elave ede bilerik ve ya sonradan da  redakte etmek mumkundur.
--her ikisini yazib icra ede bilerik,ancaq netice eyni olacaq
--1. yeni  melumatlarin elave edilmesi mekteb_id - de nezere alinir
delete from melumatlar;
insert into melumatlar(muellim_adi,muellim_soyad,tevellud,mekteb_id) 
  values('Firuz','Aliyev','01/01/1980',1);
commit;
--neticeye baxaq
select * from melumatlar;
--Buradan biz artiq , hell etmek istediyimiz yani muellimle mekteb arasindaki elaqeni teyin ede bildik.
--2.melumatlar cedvelinde var olan setrdeki mekteb_id sutununa 1 elave etmek ucun sorgu yaziriq. Gorunduyu kimi ,orada artiq deyer var, ve biz
--yeniden hemin deyeri yenileceyik.
update melumatlar set mekteb_id =1 where muellim_adi='Firuz';
commit;
--neticeni yoxlayaq . vizual hec bir deyisiklik olmamalidir. Beleki eyni deyer yeniden hemin sutuna yazilacaq
select * from melumatlar;
--Gorunduyu kimi netice  eynidir.
--Daha yeni muellim mektebde calisaq. Mustafa Poladov
--melumatlari elave edek melumatlar cedveline
insert into melumatlar(muellim_adi,muellim_soyad,tevellud,mekteb_id) 
  values('Mustafa','Poladov','01/01/1980',1);
commit;
--neticeleri yoxlayaq
select * from melumatlar;
--Her 2 muellimin mekteble elaqesini goruruk. Bu o demekdir ki, biz ne derde melumatlar cedveline yeni muellimler elave etsek de, 
--mekteb melumatlari mekteb adlo cedvelde  deyisilmez olaraq qalir.
--mekteb cedveline sorgu ederek eyani baxaq.
select * from mekteb;
--Bir haldaki, acqi sekilde mekteb cedveline insert into yazib yeni melumat elave etmemisikse o zaman burada melumatlar oz-ozune artmayacaqdir.
--Eger biz isteksek ki, melumatlar tam olaraq gorunsun yani, her muellimin hansi mektebe mexsus oldugu haqda , o mektebin ID deyeri deyil, hemin mektebin 
--butun melumatlari gorunsun, biz o zaman bunu rahatliqlar JOIN ler uzerinden ede bilerik. Elaqeli olan sutunlar uzerinden bu elaqeni quraraq lazim olan melumatlari
--gormek mumkundur.
--elaqeli sorgunu yazaq
select  mkt.*,mlt.* from mekteb mkt join melumatlar mlt on mkt.id=mlt.mekteb_id;
--burada artiq biz hansi muellimin hansi mektebde calisdiqini ve hemin mektebin hansi unvanda oldugu gore bileril. JOIN ler movzusunu daha derinden novbeti derslerimizde oyrenceyik.

--Melumatlar cedvelinin bir hissesini yani mekteb hissesini normallasdira bildik , indi ise diger hisselere dair isleri dava etdirek.

--Mekteb artiq fealiyete baslamaq uzredir ve biz shagirdleri de hemcinin qeydiyyata almaliyiq. 1a sinifine 2 shagird qeyde alaq. Hemin shagirler
--ibtidai siniflerde olduqlari ucun onlara butun fennleri Mustafa m tedris edecek.
--sorgumuzu yazaq

insert into melumatlar(mekteb_id,sinif,muellim_adi,muellim_soyad,tevellud,shagird_adi,shagird_soyad,shagird_tevellud) 
 values(1,'1A','Mustafa','Poladov','01/01/1980','Orxan','Aliev','01/01/2010');
insert into melumatlar(mekteb_id,sinif,muellim_adi,muellim_soyad,tevellud,shagird_adi,shagird_soyad,shagird_tevellud) 
 values(1,'1A','Mustafa','Poladov','01/01/1980','Nezrin','Camalova','12/03/2010');
commit; 
--neticeye baxaq
select * from melumatlar;
--her iki shagird elave edildi ve onlarin fen muellimi ve hansi mektebde oxuduqlarina aid melumatlar elave edildi. 
--Amma burada bir mesele var ki, muellime aid melumatlar shagirdlerin sayina gore tekrarlanmaga basladi.
--Bunun aradan qaldirmaq ucun mekteb -de oldugu kimi muellimleri de ayirmaq lazimdir. yani muellime aid melumatlar ayri cedvlede olmalidir ki,
--her shagirdi daxil edende melumatlar cedveline , hemin shagirde bagli olan melumatlar tekrarlanmasin.
--Bunun ucun  ilk novbede melumatlar cedvelinden muellime mexsus olan muellim_adi,muellim_soyadi ve muellim_tevellud sutunlarin silerek daha sonra bu sutunlardan ibaret 
--yeni muellim cedvelini yaratmaq lazimdir
--1.melumatlar cedvelinden qeyd olunan sutunlari silmek ucun sorgu yazaq
alter table melumatlar drop column muellim_adi;
alter table melumatlar drop column muellim_soyad;
alter table melumatlar drop column tevellud;
--neticeye baxaq
select * from melumatlar;
--neticelere diqqetle baxsaq 4 setr olan melumatlardan (2-si muellimlere aid ,2-si shagirlere aid) 2-sinde shagird melumatlari null olan setrler movcuddur.
--bu 2 setr once movcud olan muellimlere aid setrlerdir. Beleki  biz hemin setrlerde olan muellimelere sutunlara sildiyimiz ucun bele biz menzereni goruruk.
--zaten hemin muellimlerin shagirde aid olan sutunlarda deyerleri null idi. onlara aid olan melumatlar muellimlere aid sutunlarda yerlesmisdi.
--Nereze alsaq bu 2 setr harada ki shagird melumatlari null -dur, bize bir fayda vermir , biz onlari sile bilerik.
--bunun ucun sorgumuzu yazaq.
delete from melumatlar where shagird_adi is null;
commit;
--neticeye baxaq;
select * from melumatlar;
--Muellimleri elave etmek ucun yeni muellim cedvelini yaradaq
create table muellim
(muellim_adi varchar2(100),
 muellim_soyad varchar2(100),
 tevellud varchar2(100));
--neticeye baxaq
select  * from muellim;
--yeni cedvelimizi yaratdiq amma bir meseleni unutduq, neceki, mektebi de ayiranda elaqendirmeni qurmusduq burada da bunu etmek lazim olacaq.
--1.muellim cedvelinde yeni sutun elave edek.
alter table muellim add id varchar2(100);
--2.melumatlar cedveline yeni sutun elave edek
alter table melumatlar add muellim_id varchar2(100);
--neticeye baxaq
select * from muellim;
--neticeye baxaq 
select * from melumatlar;
--daha once melumatlar cedveline elave etdiyimiz her iki muellime aid melumatlari yeni cedvele yani muellim adli cedvele elave edek.
--amma bundan evveli deyismek istediyim bir meseleni hell etmek isteyirem. bu muellim cedvelinde olan sutun adlarinda muellim sozunun tekrar olunmasidir.
--mence bu artiqdir ,beleki onsuzda sutunun hansi cedvele aid oldugunu mueyyen ede bilikse, o zaman bu adin tekrarlanmasina ehtiyyac qalmir ve bu [sadece]  artiqliqdir.
--odur ki, ilk once hemin sutunlari rename edek daha sonra muellimlere aid melumatlari elave edek
alter table muellim rename column muellim_adi to ad;
alter table muellim rename column muellim_soyad to soyad;
--neticeye baxaq
select * from muellim;
--sutun adlari deyisdirildi ,indi ise muellim melumatlarini elave edek
insert into muellim(ad,soyad,tevellud,id) values('Firuz','Aliyev','01/01/1980',1);
insert into muellim(ad,soyad,tevellud,id) values('Mustafa','Poladov','01/01/1980',2);
commit;
--neticeye baxaq
select * from muellim;
--bu melumatlar esasinda melumatlar cedvelinden shagirdleri hansi muellim tedris etdiyini teyin etmek ucun sorgu yazaq
--qeyd etmisdiki ki, her 2 shagirde Mustafa m ders deyir. Odur ki, her iki shagirdin muellim_id si 2  deyerini alacaqdir.
--ilk novbede melumatlar cedveline nezer salaq
select * from melumatlar;
--redakte ucun sorgumuz yazaq
update melumatlar set muellim_id=2 where shagird_adi in ('Orxan','Nezrin');
commit;
--neticeye baxaq 
select * from melumatlar;
--burada biz muellim_id sutunda teleb olunan muellim ID ni goruruk.
--Neye gore update sorgumuzda where bolmesini istife etdik bir halda ki, hemin cedvelde 2 setr melumat var idi ve eger biz
--update melumatlar set muellim_id=2; yazsaydiq ne olardi?
--Eslinde netice eyni olacaqdi. Amma burada bir mesele var ki, ona diqqet etmek lazimdir. Eger
--Update emrini istifade etdikde eger sert tetbiq olunmursa, o hemin cedvelde olan butun setrlerde muellim =2  tetbiq edecek.
--bizim meselemizde ele bir qorxusu yoxdur ona gore ki ora da sadece 2 setr melumat var idi ve biz mehz bu ikisini deyismek isteyirdik.
--ancaq biz indiden ozumu update emrinde setrleri filtr etmek verdisini qazanmasaq gelecekde bu bize baha basha gele biler.
--Yani lazim olmayan setrlerde de melumatlar deyisile biler. Bu sebebden biz update zamani mutleq sekilde hansi setrlerde bu deyisiklik olmalidirsa,
--onlari filtr bolmesinde teyin etmeliyik , ferq etmez cedvelde nece sayda setr var 2 yoxsa 200 000 sayda.


--Eger biz hansi muellimin hansi shagirdleri oldugunu gormek istesek  o zaman muellim ve melumatlar cedvellerini elaqelendirerk bunu gore bilerik
select m.*,mlt.* from muellim m join  melumatlar mlt on m.id=mlt.muellim_id;
--Eger biz eyni zamanda mekteb melumatlarini da gormek istesek o zaman mekteb cedvelini de burada elave etmek lazim olacaq
select mkt.*,m.*,mlt.* from melumatlar mlt
  join  mekteb mkt on mkt.id=mlt.mekteb_id
  join muellim m on mlt.muellim_id=m.id; 
  
 --Bu sorgunun neticesinde biz shagirdi olmayan Firuz m gormuruk buna sebeb,Firuz m shagirdi olmadigindan onun ID sinin melumatlar cedvelinde olmamasidir. Eger
 --bunu nezere alib yuxaridaki sorgunu deyisek o zaman biz hemcinin Firuz m  gore bileceyik. JOIN ler movzusunu daha derinden novbeti dersler kececeyik
select mkt.*,m.*,mlt.* from melumatlar mlt
  join  mekteb mkt on mkt.id=mlt.mekteb_id
  right join muellim m on mlt.muellim_id=m.id; 

 --burada right sozunu elave etmekle ustunluk derecesini daha cox muellim cedveline vermis oluruq ve Firuz m burada gore bilirik.
 --Lakin burada gorunse de qeyd etidiyimiz ki, hemin muellime shagird baglanmadigindan Firuz m islediyi mekteb melumatlari gorunmur. Halbuki onun da hemin mektebde
 --calisdigi melumdur. Bunu hell etmek ucun biz nece ki melumatlari mekteb ve muellim cedvelleri ile elaqelendirdik, elece de  muellim ve mekteb cedvellerin
 --arasinda da elaqe yaratmaq lazimdir.
 --bunun ucun mekteb cedvelinde deyisikliye ehtiyyac yoxdur , beleki muellim mektebden asili oldugu ucun mekteb cedvelinden artiq movcud olan ID sutuna 
 --istinad edecek, muellim cedvelinde mekteb_id sutunu yaradaraq orada mektebin ID deyerini yazmaq lazimdir
--sutunu elave edek
alter table muellim add mekteb_id varchar2(100);
--deyeri elave edek.
update muellim set mekteb_id=1 where ad in ('Firuz','Mustafa');
commit;
 --neticeye baxaq
select * from muellim;
--Mustafa m adi gorunurdu niye onun ucun de  bu deyeri elave etdik hemcinin ,kimi sual yaranarsa, 
--Mustafa m adinin gorunmesi shagirde bagli oldugu ucun  ve gelecekde bu baglantinin olmamasi ehtimalinin olmasi nezere alaraq, butun hallarda 
--mektebe  bagli muellimleri de gormemiz ucun bunu etdik. Yani muellime shagird baglidir ya bagli deyil, biz bu elaqe uzerinden deyil
--mehz mekteb muellim cedvellerinin elaqesi uzerinden meseleye yanashma etmeyimiz daha dogrudur.
--mekteb ve muellim 2 cedvel uzre butun melumatlari gormek ucun sorgu yazaq
select mkt.*,m.* from mekteb mkt join muellim m on mkt.id=m.mekteb_id;  


--Melumatlar cedveline qayidaq ve bir shagirdin dersden qiymet almasini cedvele elave edek
--melumatlar cedveline nezer salaq
select * from melumatlar;
--Orxan adli sagirdin 5 qiymeti almasini  ,melumatlar cedveline elave edek
insert into melumatlar(sinif,shagird_adi,shagird_soyad,shagird_tevellud,shagirs_qiymet,qiymet_tarix,mekteb_id,muellim_id)
values('1A','Orxan','Aliyev','01/01/2010',5,'05/15/20223',1,2);
commit;
--neticeye baxaq
select * from melumatlar;
--neticece Orxanin dersden qiymet almasi onun butun melumatlarinin tekrar olaraq  melumatlarlar cedeveline yazilmasina getirir. Bu da melumat artiqligidir.
--bunu aradan qaldrimaq ucun diger cedveller kimi tekrarlanan melumatlarin  bu cedvelden cixarilmasini icra etmek lazimdir. 
--bizim halda bunlar shagird_adi , shagird_soyad ve tevelluddur.
--lazim olan cedveli yaradaq. burada muellim cedvelinde oldugu kimi cedvelinin adini ozunde dasiyan sutun adlarinda nezere almiriq.
create table shagird
(ad varchar2(100),
 soyad varchar2(100),
 tevellud varchar2(100));

--neticeye baxaq
select * from shagird;

--melumatlar cedvelinde olan shagirde aid sutunlari silek.
alter table melumatlar  drop column shagird_adi;
alter table melumatlar drop column shagird_soyad;
alter table melumatlar drop column shagird_tevellud;
  
--neticeye baxaq;
select * from  melumatlar;
 
 
--shagirdleri shagird cedveline elave edek 
insert into shagird(ad,soyad,tevellud) values('Orxan','Aliyev','01/01/2010');
insert into shagird(ad,soyad,tevellud) values('Nazrin','Camalova','12/03/2010');
commit;

--neticeye baxaq
select * from shagird;

--melumatlar cedvelinden  her shagirdin aldigi qiymetleri shagird cedveli ile elaqelendirmek lazimdir. bunun ucun her iki cedvelde elave edilmelidir.
--melumatlar cedvelindeki qiymetlerin shagirde bagli oldugunu nezere alsaq, shagird cedvelinde ID sutunu elave edilmeli ve bu sutuna istinad edecek,
--melumatlar cedvelinde SHAGIRD_ID sutunu elave edilmelidir.
--shagird cedveline elave sutun yaradaq
alter table shagird add id varchar2(100);
--melumat cedveline elave sutun yaradaq
alter table melumatlar add shagird_id varchar2(100);
--neticeye baxaq;
select * from shagird;
--her setr ucun ID deyer verek
update shagird set id=1 where  ad='Orxan';
update shagird set id=2 where ad='Nazrin';
commit;
--neticeye baxaq
select * from shagird;

--melumatlar cedvelinde elave etdiyimiz Orxan ucun yeni setrde haradaki, qiymet teyin edilib hemin setr ucun SHAGIRD_ID sutuna ORxana aid deyeri elave edek.
update melumatlar set shagird_id=1 where shagirs_qiymet=5 and qiymet_tarix='05/15/20223';
commit;

--neticeye baxaq
select * from melumatlar;

--SHagird_id sutununa deyer elave edildi diger 2 setr evvelki 2 sayda elave edilen shagirdlere aid oldugu ucun onlara sile bilerik.
delete from melumatlar where qiymet_tarix is null;
commit;

--neticeye baxaq
select * from melumatlar;

--Orxan adli shagirdin daha bir qiymet almasini qeyd edek
insert into melumatlar(sinif,shagirs_qiymet,qiymet_tarix,mekteb_id,muellim_id,shagird_id)
values('1A',4,'05/16/2023',1,2,1);
commit;
 
--neticeye baxaq
select * from melumatlar;
--burada her defe sinif adinin tekrarlanmasini goruruk, bu sebebden de sinifin de 
